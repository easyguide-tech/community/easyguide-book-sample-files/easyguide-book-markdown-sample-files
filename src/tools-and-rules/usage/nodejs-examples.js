// node.js, "classic" way:
var MarkdownIt = require("markdown-it"),
  md = new MarkdownIt();
var result = md.render("# markdown-it rulezz!");
  
// node.js, the same, but with sugar:
var md = require("markdown-it")();
var result = md.render("# markdown-it rulezz!");
  
// browser without AMD, added to "window" on script load
// Note, there is no dash in "markdownit".
var md = window.markdownit();
var result = md.render("# markdown-it rulezz!");
  
// Single line rendering, without paragraph wrap:
var md = require("markdown-it")();
var result = md.renderInline("__markdown-it__ rulezz!");

// Plugins load
var md = require('markdown-it')()
            .use(plugin1)
            .use(plugin2, opts, ...)
            .use(plugin3);
