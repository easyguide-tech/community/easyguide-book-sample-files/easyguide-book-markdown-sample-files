---
output:
  custom_document:
    toc: true
    toc_depth: 2
    path: ./exports/document.html
    pandoc_args: ["--dpi=300", "--highlight-style=tango",--metadata=keywords:'document'"]
---

# Pandoc HTML

```bash
pandoc --from markdown --to html5  -o "./exports/document.html"  --filter pandoc-plantuml-filter  "sample.md"
```
