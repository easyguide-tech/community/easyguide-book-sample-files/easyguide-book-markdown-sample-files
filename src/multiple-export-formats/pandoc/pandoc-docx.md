---
output:
  word_document:
    toc: true
    toc_depth: 2
    path: ./exports/document.docx
    pandoc_args: ["--dpi=300", "--highlight-style=tango",  --metadata=keywords:'document'"]
---

# Pandoc DOCX

```bash
pandoc --from markdown --to docx  -o "./exports/document.docx"  --filter pandoc-plantuml-filter  "sample.md"
```
