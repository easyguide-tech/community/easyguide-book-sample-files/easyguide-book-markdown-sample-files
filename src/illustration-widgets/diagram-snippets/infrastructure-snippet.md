```plantuml
  
  'SETTINGS
  '---
  'scale 720 width
  
  allow_mixing
  
  
  'HEADER
  '---
  title
  <font color=white> Frontend : Package/component </font>
  end title
  
  legend top right
  <font color=red>Objective : </font> Overview of the system's folders/files.
  end legend
  
  'Title custom style
  skinparam titleBorderRoundCorner 0
  skinparam titleBorderThickness 1
  skinparam titleBackgroundColor red
  skinparam titleFontColor white
  skinparam titleFontSize 24
  
  'legend custom style
  skinparam legendBorderRoundCorner 0
  skinparam legendBorderThickness 0
  skinparam legendFontSize 14
  skinparam legendFontColor white
  skinparam legendBackgroundColor darkGrey
  
  
  'BODY
  '---
  
  package angular_core as "Angular Core" {
  }
  
  angular_core -right-> module
  
  package module as "feature.module.ts"  {
  agent "feature.interface.ts" as interface  
  agent "feature.model.ts" as model 
  agent "feature.service.ts" as service 
  agent "feature-routing.module.ts" as route 
  route --> component
  
  package component  as "feature.component.ts"  {
  agent "feature.template.html" as template 
  agent "feature.style.scss" as style 
  agent "feature.controller" as controller 
  }
  
  
  
  template --> style
  template --> controller
  model --> controller
  service --> model
  controller --> service
  service <-- interface
  
  }
  
  ```
