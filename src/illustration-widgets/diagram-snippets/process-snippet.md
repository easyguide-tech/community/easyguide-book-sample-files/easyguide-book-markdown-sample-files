```plantuml
  
  'SETTINGS
  '---
  'scale 720 width
  'left to right direction
  
  'Title custom style
  skinparam titleBorderRoundCorner 0
  skinparam titleBorderThickness 1
  skinparam titleBackgroundColor red
  skinparam titleFontColor white
  skinparam titleFontSize 24
  
  'legend custom style
  skinparam legendBorderRoundCorner 0
  skinparam legendBorderThickness 0
  skinparam legendFontSize 14
  skinparam legendFontColor white
  skinparam legendBackgroundColor darkGrey
  
  'HEADER
  '---
  title
  <font color=white> Frontend : Sequence diagram </font>
  end title
  
  legend top right
  <font color=red>Objective : </font> Overview of the system's process.
  end legend
  
  
  
  'BODY
  '---
  
  'custom settings
  hide footbox
  
  actor user
  
  box "app Frontend" #lightBlue
      participant "templateView \n(router-outlet)" as templateView <<View>>
      participant featureRouterModule <<Route>>
      participant featureComponent <<ViewModel>>
      participant featureModel <<Model>>
      participant featureService #lightGreen
      participant IdataJson  <<interface>> #lightGreen
  end box
  
  
  box "api Backend  \n(data or web service)" #lightGreen
      participant featureApi #lightGreen
  end box
  
  user -> templateView : Interaction
  activate user
  activate templateView
  templateView -> featureRouterModule : Request partial view \n and handle events
  activate featureRouterModule
  
  featureRouterModule -> featureComponent : Load component
  destroy featureRouterModule
  activate featureComponent
  
  featureComponent -> featureService : Method call service
  activate featureService #lightGreen
  
  featureComponent -> featureModel : Method call data
  activate featureModel
  
  
  
  featureService -[#darkGreen]> featureApi : Method call httpClient
  activate featureApi #lightGreen
  
  featureApi -[#darkGreen]> IdataJson : Return JSON data
  deactivate featureApi
  activate IdataJson #lightGreen
  
  IdataJson -[#darkGreen]> featureService : Return Object <IdataJson>
  deactivate IdataJson
  
  featureService -[#darkGreen]> featureService : <back:darkGreen><color:white>Adapt IdataJson for featureModel</color></back>\n<back:darkGreen><color:white>with a transform function</color></back>
  activate featureService #lightGreen
  deactivate featureService
  
  
  featureService -[#darkGreen]> featureModel: Update featureModel
  deactivate featureService
  
  
  featureModel -> featureComponent : Update data properties
  
  featureComponent -> templateView : Render template view \n and Bind data properties
  deactivate featureModel
  deactivate featureComponent
  
  templateView -> user : interaction
  deactivate templateView
  deactivate user
  
  ```
