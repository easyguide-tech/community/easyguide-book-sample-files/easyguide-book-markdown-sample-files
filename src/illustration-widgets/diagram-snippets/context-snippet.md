```plantuml
  
  'SETTINGS
  '---
  'scale 720 width
  left to right direction
  
  'Title custom style
  skinparam titleBorderRoundCorner 0
  skinparam titleBorderThickness 1
  skinparam titleBackgroundColor red
  skinparam titleFontColor white
  skinparam titleFontSize 24
  
  'legend custom style
  skinparam legendBorderRoundCorner 0
  skinparam legendBorderThickness 0
  skinparam legendFontSize 14
  skinparam legendFontColor white
  skinparam legendBackgroundColor darkGrey
  
  'HEADER
  '---
  title
  <font color=white> Architecture overview : Deployment diagram </font>
  end title
  
  legend top right
  <font color=red>Objective : </font> Overview of the system's architecture.
  end legend
  
  
  'BODY
  '---
  
  node "Personal computer" as Pc <<device>> {
      node "Web browser" as WebBrowser <<software>> {
          component "Angular frontend" as Angular <<web-app>>
      }
  }
  
  
  
  node "Web server" as WebServer <<device>> {
      component "Java backend" as Java <<web-api>>
  }
  
  
  cloud "Cloud service" as CloudService <<device>> #darkGrey {
      artifact "Rest web service" as WebService <<web-api>>
  }
  
  database "Database server" as Database <<device>> #red {
      storage "MySql data store" as MySql <<storage>>
  }
  
  Angular --(0--- Java : <font color=red>HTTP REST </font> <<protocol>>
  Java --(0-- MySql : <font color=red>TCP/IP</font> <<protocol>>
  Java --(0-- WebService : <font color=red>HTTP REST</font> <<protocol>>
  
  ```
